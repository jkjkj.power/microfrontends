import React from "react";

// Импорт компонентов из вне
import { Basket } from "basket/Basket";
import { Button } from "basket/Button";
import { Recomendations } from "recos/Recomendations";
// -----

import { models } from "../../data";
import "./Main.css";

export const Main = () => {
  const [selectedModel, setSelectedModel] = React.useState(models[0]);
  const [basketItemsCount, setBasketItemsCount] = React.useState(0);

  const handleModelClick = (model) => {
    setSelectedModel(model);
  };

  const handlwBuyClick = () => {
    setBasketItemsCount(basketItemsCount + 1);
  };

  return (
    <div className="container">
      <header className="header">
        <h1>The Model Store</h1>
        <div>
          <Basket itemsCount={basketItemsCount} />
        </div>
      </header>
      <ModelsContainer selectedModel={selectedModel} onclick={handleModelClick}>
        <Recomendations model={selectedModel.sku} />
      </ModelsContainer>
      <div className="button-wrap">
        <Button onClick={handlwBuyClick} price={selectedModel.price} />
      </div>
    </div>
  );
};

const ModelsContainer = ({ selectedModel, onclick, children }) => (
  <div className="modelsWrap">
    <img className="model" src={selectedModel.image} />
    <div className="modelsContainer">
      <h2>{selectedModel.name}</h2>
      <Models selectedModel={selectedModel} onClick={onclick} />
    </div>
    {children}
  </div>
);

const Models = ({ onClick, selectedModel }) => (
  <div className="relatedProducts">
    {models.map((model, index) => {
      return (
        <img
          className={
            selectedModel.name === model.name ? "selectedImage" : "image"
          }
          key={index}
          onClick={() => onClick(model)}
          src={model.thumb}
        />
      );
    })}
  </div>
);
