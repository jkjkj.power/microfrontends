const webpack = require("webpack");
const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");

const { ModuleFederationPlugin } = webpack.container;

const deps = require("./package.json").dependencies;

module.exports = {
  devServer: {
    hot: true,
    port: 3000,
  },
  entry: "./src/index.jsx",
  output: {
    path: path.resolve("dist"),
    filename: "bundle.js",
  },
  resolve: {
    extensions: [".js", ".jsx"],
  },
  module: {
    rules: [
      {
        test: /\.js$|jsx/,
        exclude: /(node_modules|bower_components)/,
        use: {
          loader: "babel-loader",
          options: {
            presets: ["@babel/preset-env"],
          },
        },
      },
      {
        test: /\.(jpg|png|gif|woff|eot|ttf|svg)/,
        use: {
          loader: "file-loader", 
        },
        
      },
      {
        test: /\.css$/i,
        use: ["style-loader", "css-loader"],
      },
    ],
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: "./src/index.html",
      scriptLoading: "defer",
    }),
    new ModuleFederationPlugin({
      name: "shell",
      filename: "shell.js",
      // Путь к компонентам из вне
      remotes: {
        basket: "basket@http://localhost:3001/basket.js",
        recos: "recos@http://localhost:3002/recos.js",
      },
    }),
  ],
};
