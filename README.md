Microfrontends example using React and webpack Module federation plugin

hot to start:
- cd to *shell-team* folder
- *npm run deps-install*
- *npm run dev*

In each folder, an independent application is part of the main one, respectively, each can be launched separately.
