import React from "react";
import { Basket } from "./components/Basket/Basket";
import { Button } from "./components/Button/Button";

const App = () => {
  return <div>
    <Basket />
    <Button />
  </div>;
};

export default App;
