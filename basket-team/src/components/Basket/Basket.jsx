import React from "react";
import "./Basket.css";

export const Basket = ({ itemsCount = 0 }) => {
  return (
    <div className="basket-container">
      <div className="basket">Basket: {itemsCount} item(s)</div>
    </div>
  );
};
