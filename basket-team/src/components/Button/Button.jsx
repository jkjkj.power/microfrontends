import React from "react";
import "./Button.css";

export const Button = ({ price, onClick }) => {
  return (
    <div className="button-container">
      <button onClick={onClick} className="button">
        buy for {price}
      </button>
    </div>
  );
};
