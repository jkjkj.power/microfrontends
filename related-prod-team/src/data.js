import reco_1 from "./images/reco_1.jpg";
import reco_3 from "./images/reco_3.jpg";
import reco_4 from "./images/reco_4.jpg";
import reco_5 from "./images/reco_5.jpg";
import reco_6 from "./images/reco_6.jpg";
import reco_7 from "./images/reco_7.jpg";
import reco_8 from "./images/reco_8.jpg";

export const recomendations = {
    t_porsche: [reco_3, reco_5, reco_6],
    t_fendt: [reco_3, reco_6, reco_4],
    t_eicher: [reco_1, reco_8, reco_7],
  }