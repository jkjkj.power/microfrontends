import React from 'react';
import { recomendations } from '../../data';
import "./Recomendations.css"


export const Recomendations = ({model}) => {
    return (
        <div className="recos">
            {recomendations[model]?.map((image, index) => <img key={index} src={image}/>)}
        </div>
    )
}
